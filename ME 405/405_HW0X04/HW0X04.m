%% Define system parameters
Rm = 60;     %mm
lr = 50;     %mm
rb = 105;    %mm
rg = 42;     %mm
lp = 110;    %mm
rp = 325;    %mm
rc = 50;     %mm
mb = 30;     %g
mp = 400;    %g
Ip = 1.88e6; %g*mm^2
b  = 10    ; %mN-m*s/rad 

%% Define state-space Matrices
A = [0 0 1 0; 0 0 0 1; -5.217 4.011 0 0; 112.9 64.83 0 0]; %define A matrix
B = [0; 0; 32.5; -703.2]; %define B matrix
C = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1]; %define C matrix
D = [0; 0; 0; 0]; %define D matrix

%% Open Loop
ICs = [0 5; 0 0; 0 0; 0 0]; %establish IC's for case A an B in idividual columns
tstep   = .0001; %step size to be used [s]
tstop = [1, 0.4]; %length of simulation for case a and b
sys     = ss(A,B,C,D); %create state-space system
axes    = ["x (cm)", "${\theta_x}$ (rad)", '$\dot{\theta_x}$ (rad/s)', '$\dot{x}$ (cm/s)']; %vector containing strings for axes labels in latex format
titles  = ["Ball Position" "Table Angle" "Ball Velocity" "Table Angular Velocity"]; %strings for plot titles
for n = 1:2 %for case a and b
    t = 0:tstep:tstop(n); %define time vector
    u  = zeros(size(t(1,:))); %the motor torque input is zero
    [Y,T] = lsim(sys,u,t,ICs(:,n)); %simulate the system with zero input, for the given amount of time, at the given ICs
    figure(n) %create new figure
    for i = 1:4 %for each output
        subplot(2,2,i) %create 2x2 subplot
        plot(T, Y(:, i)) %plot outputs as subplots
        title(titles(i)) %title
        ylabel(axes(i), 'Interpreter', 'latex') %Label axes
        xlabel("Time (s)")
    end
end

%% Closed Loop
K = [-.3 -.3 -.05 -.02]; %vector containing the gains as given by Charlie
sys_cl = ss(A-B*K,B,C,D); %A = A-u and U=B*K
tstep = .0001;
tstop = 10;
t = 0:tstep:tstop;
u = zeros(size(t(1,:)));
figure(3)
[Y,T] = lsim(sys_cl,u,t,ICs(:,2));
for i = 1:4
    subplot(2,2,i)
    plot(T, Y(:, i))
    title(titles(i))
    ylabel(axes(i), 'Interpreter', 'latex')
    xlabel("Time (s)")
end
     