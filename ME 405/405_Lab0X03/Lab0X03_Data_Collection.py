'''
@file       Lab0x03_Data_Collection.py
@brief      Contains a task that collects ADC values for a mechanical push-button

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/ME%20405/405_Lab0X03/
@author     Caleb O'Gorman
@date       05/09/2021
'''

import pyb
from array import array

class taskData:
    '''
    @brief a finite state machine class that controls data collection for a step response
    '''
    
    S0_WAITING           = 0 ## Constant defining State 0 - Waiting for char
    
    S1_COLLECT_DATA      = 1 ## Constant defining State 1
    
    S2_SEND_DATA         = 2 ## Constant defining State 3
    
    adc = pyb.ADC(pyb.Pin.board.A4) #adc pin connected to button pin
    
    frequency = 2000000 #frequency of the adc read
     
    leng = int(frequency/700) #desired length of data array
    
    tim = pyb.Timer(2, freq = frequency) #timer for adc read
    
    buf = array('H', (0 for index in range (leng))) #allocate memory for adc values
    
    def __init__(self): 
        '''
        @brief constructor for objects of the data collection class'''
        
        self.state    = self.S0_WAITING # will start at state 0
        self.myuart   = pyb.UART(2)
        self.flag     = 0 #flag that will be set high on button release
        
    def run(self):
        '''@brief method that runs the finite state machine'''
        if self.state == self.S0_WAITING: # state 0 waits for user to send a 'g'
            val = '' # intialize variable that will correspond to the 'g'
            if self.myuart.any() != 0: # if user sends something to UART terminal
                val = self.myuart.read().decode() # than read it and set it to val
                if val == 'g':
                    self.state  = self.S1_COLLECT_DATA  #transition states
                
        if self.state   == self.S1_COLLECT_DATA: # state 1 calls function to collect data
            if self.flag == 1: #if button is realeased
                self.adc.read_timed(self.buf, self.tim)
                self.state  = self.S2_SEND_DATA # transition states
                self.flag = 0 
            
        if self.state == self.S2_SEND_DATA:      # state 2 sends data back to the serial port
            self.myuart.write('{:}\r\n'.format(len(self.buf))) # 1st sends length of arrays
            for n in range(len(self.buf)):     # for the length of the array, send data and times as csv
                self.myuart.write('{:},{:}\r\n'.format(self.buf[n]*3.3/4095, (n*1000000/(self.frequency)))) #maps data from 0-495 to 0-3.3V 
            self.state = self.S0_WAITING #wait for another trial
            
    def buttonPress(self, IRQ_src): #callback ti be executed upon button release
        self.flag = 1 #set flag high
            
if __name__ == "__main__":
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13) #button pin

    task = taskData() #create task object

    #Interrupt handler for the callback triggered by the button release
    buttonPress = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING,
                             pull=pyb.Pin.PULL_NONE, callback=task.buttonPress)
    
    while True: #runs the task indefinitely
        task.run()
            
    
