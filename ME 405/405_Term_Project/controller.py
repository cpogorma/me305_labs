''' 
@file       controller.py
@brief      Closed loop controller class that integrates full-state feedback
            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/ME%20405/405_Term_Project/
@author     Caleb O'Gorman
@date       06/03/2021'''
import pyb
import utime
import math

class controller:
    '''
    @brief A closed loop controller for the ME 405 board using full-state feedback
    @details This controller is meant to balance a ball in the center of 
    a two degree of freedom rotating table. See the following link for more 
    information: https://cpogorma.bitbucket.io/term_project_405.html'''
    
    def __init__(self, kx, ky, enc1, enc2, moe1, moe2, touch, interval):
        '''
        @brief          Constructor that creates a control system for a 2 DOF table
        @param kx       A 4x1 vector containing gains for the x-dimension
        @param ky       A 4x1 vector containing gains for the y-dimension
        @param enc1     Encoder for the x-dimension motor
        @param enc2     Encoder for the y-dimension motor
        @param moe1     Motor for the x-dimension
        @param moe2     Motor for the y-dimension
        @param touch    Resistive touch panel
        @param interval The interval at which the function will run
        '''
        self.kx=kx
        self.ky=ky
        self.enc1=enc1
        self.enc2=enc2
        self.moe1=moe1
        self.moe2=moe2
        self.touch=touch
        self.interval=interval
        
        
        self.start = utime.ticks_ms()
        self.next  = utime.ticks_ms() + interval