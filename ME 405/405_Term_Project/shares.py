# -*- coding: utf-8 -*-
''' @file shares.py
@brief      Contains position and val variables that will be shared globally

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/Lab0xFF/
@author     Caleb O'Gorman
@date       03/10/2021'''


position = 0
val = ''