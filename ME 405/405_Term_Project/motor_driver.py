''' 
@file motor_driver.py
@brief      Contains a motor driver class to create motor objects

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/ME%20405/405_Term_Project/
@author     Caleb O'Gorman
@date       06/3/2021'''

import pyb
from pins import motors

class motorDriver:
    '''
    @brief This class implements a motor driver for the
            ME405 board. '''
    global nFault_flg #global variable that goes high on a fault
    fault_flg = 0 #initialize at zero

    def __init__ (self, n):
        ''' 
        @brief    Creates a motor object by initializing GPIO
                  pins and turning the motor off for safety.
        @param n Index that corresponds to the number of the motor being created.
                  If first motor, n=0, second: n=1'''
        self.nSLEEP_pin = motors[n][0] #gets n-sleep pin from pins file
        self.IN1_pin    = motors[n][1] #gets input pin from pins file
        self.IN2_pin    = motors[n][2] #gets input pin from pins file
        self.timer      = motors[n][3] #gets timer from pins file
        self.ch1        = motors[n][4] #gets ch1 number from pins file
        self.ch1        = motors[n][5] #gets ch2 number from pins file
        self.nFault_pin = motors[n][6] #gets nFault pin from pins file
        self.button_pin = motors[n][7] #gets button pin from pins file
        self.nSLEEP_pin.low()
        if n==0: #if this is the first motor that is created, set up external interrupts
            self.nFaultInt = pyb.ExtInt(self.nFault_pin,mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE,callback=self.nFault)
            self.ButtonInt = pyb.ExtInt(self.button_pin,mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE,callback=self.clearFault)
        else:
            pass
        
    def enable(self):
        '''
        @brief Enables the motor'''
        global nFault_flg    
        if nFault_flg == 0: #if nfault flag is low
            self.pin_nSLEEP.high() #set enable pin high
        else: #if theres fault print this warning
            print('Please clear fault and press blue button')
        
    def disable(self):
        '''
        @brief disables the motor'''
        self.nSLEEP_pin.low() #sets the enable pin low
    
    def nFault(self, IRQ):
        '''
        @brief triggers when the nfault pin goes high and disables motors
        @param IRQ necessary parameter for external interrupt'''
        global nFault_flg
        if self.pin_nFault.value() == 0: #filters out false function calls
            nFault_flg = 1 #sets fault flag 1
            self.disable() #disables motor
            print('Fault detected. Please clear and then press blue button.') #prints warning message
        else: 
            pass
        
    def clearFault(self, IRQ):
        '''
        @brief  Takes the motor out of the fault state
        @param  placeholder variable to satisfy callback function requirement'''
        global nFault_flg
        if nFault_flg == 0: #filters out false function calls
            pass
        else:
            nFault_flg = 0 #clears flag
            self.enable()  #reenable motors
            print('Fault cleared.') #notify user
        
    def set_duty(self, duty):
        ''' 
        @brief  This method sets the duty cycle to be sent
                to the motor to the given level. Positive values
                cause effort in one direction, negative values
                in the opposite direction.
        @param duty A signed integer holding the duty
                    cycle of the PWM signal sent to the motor '''
        if duty == 0: #stop motor if duty is zero
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(0)
        elif duty < 0: #move motor backwards
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(abs(duty))
        elif duty > 0: #move motor forwards   
            self.ch1.pulse_width_percent(duty)
            self.ch2.pulse_width_percent(0)
        else:
            pass
            
if __name__ == '__main__':
# Adjust the following code to write a test program for your motor class. Any
    # Create a motor object passing in the pins and timer
    moe = motorDriver(1)
    # Enable the motor driver
    moe.enable()