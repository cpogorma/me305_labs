'''
@file       touch_screen_driver.py
@brief      Contains a driver for a 2-axis resistive touch panel 
@details    Module contains methods to determine a point of contacts x-axis
            dimension, y-axis dimension and wether or not something is contacting
            the glass it all. It also contains a method readAll to scan all
            dimensions, and return a boolean value corresponding to wether or 
            not something is touching the glass.

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/ME%20405/405_Term_Project/
@author     Caleb O'Gorman
@date       06/3/2021
'''

import pyb
import utime
from pins import touch

class Touch:
    '''
    @brief a hardware drive for a resistive touch panel
    '''
    length=7   #the long dimension of the touch panel (x-dimension)
    width=4.5  #the short dimension of the touch panel (y-dimension)
    center=[length/2,width/2] #the panel's center, measured from the datum

    def __init__(self):
        '''
        @brief A constructor to create a touch-panel object
        '''
        self.xm = touch[0] #pin corresponding the x-minus reading
        self.xp = touch[1] #pin corresponding the x-plus  reading
        self.ym = touch[2] #pin corresponding the y-minus reading
        self.yp = touch[3] #pin corresponding the y-plus  reading
        
    def scanX(self):
        '''
        @brief   Scans and returns the x location in cm
        @details This method first initializes the pins in the correct modes,
                 and then creates and uses the appropriate ADC object to get a
                 digital reading which it then converts tothe x location. 
                 It has a buffer period of 5 microseconds.
        @return  x location (cm)
        '''
        self.yp.init(mode=pyb.Pin.IN) #configures yp pin in input mode
        self.ym.init(mode=pyb.Pin.IN) #configures ym pin in input mode
        self.xm.init(mode=pyb.Pin.OUT_PP,value=0) #sets xm pin output mode low
        self.xp.init(mode=pyb.Pin.OUT_PP,value=1) #sets xp pin output mode high
        self.ym.init(mode=pyb.Pin.ANALOG) #sets ym to take analog inputs
        ADC_ym=pyb.ADC(self.ym)  #creates ADC object
        ratio=ADC_ym.read()/4095 #scales the adc reading
        dim=ratio*self.length    #swtiches units to length
        start=utime.ticks_us()   #stamps buffer start time
        while utime.ticks_us()<start+5: #5 microsecond buffer period
            pass
        return dim-self.center[0] 
    
    def scanY(self):
        '''
        @brief   Scans and returns the y location in cm
        @details This method first initializes the pins in the correct modes,
                 and then creates and uses the appropriate ADC object to get a
                 digital reading which it then converts to the y location. 
                 It has a buffer period of 5 microseconds.
        @return  y location (cm)
        '''
        self.ym.init(mode=pyb.Pin.OUT_PP,value=0) #sets ym pin output mode low
        self.yp.init(mode=pyb.Pin.OUT_PP,value=1) #sets yp pin output mode high
        self.xp.init(mode=pyb.Pin.IN) #configures xp pin in input mode
        self.xm.init(mode=pyb.Pin.IN) #configures mm pin in input mode
        self.xm.init(mode=pyb.Pin.ANALOG) #sets xm to take analog inputs
        ADC_xm=pyb.ADC(self.xm) #creates ADC object
        ratio=ADC_xm.read()/4095  #creates ADC object
        dim=ratio*self.width      #swtiches units to length
        start=utime.ticks_us()    #stamps buffer start time
        while(utime.ticks_us()<start+4): #5 microsecond buffer period
            pass
        return dim-self.center[1]
        
    def scanZ(self):
        '''
        @brief   Determines wether something is contacting the touch-panel
        @details This method first initializes the pins in the correct modes,
                 and then creates and uses the appropriate ADC object to determine
                 whether something is in contact with the touch panel.
                 It has a buffer period of 5 microseconds.
        @return  boolean representing if something is on touch panel
        '''
        self.xm.init(mode=pyb.Pin.OUT_PP,value=0) #sets xm pin output mode low
        self.yp.init(mode=pyb.Pin.OUT_PP,value=1) #sets yp pin output mode high
        self.xp.init(mode=pyb.Pin.IN)  #configures xp pin in input mode
        self.ym.init(mode=pyb.Pin.ANALOG) #sets ym to take analog inputs
        ADC_ym=pyb.ADC(self.ym) #creates ADC object
        start=utime.ticks_us()  #stamps buffer start time
        while(utime.ticks_us()<start+5): #5 microsecond buffer period
            pass
        if(ADC_ym.read()>=3500): #if seomthing is contacting touch panel
            return False
        else:
            return True
           
    def readAll(self):
        '''
        @brief   Scans all three axes and prints elapsed time
        @return  tuple containing x pos, y pos, and z boolean
        '''
        start=utime.ticks_us() 
        x=self.scanX() #scans x
        y=self.scanY() #scans y
        z=self.scanZ() #scans z
        print('Time taken: {}'.format(utime.ticks_us()-start)) #prints elapsed t
        return (x,y,z)


if __name__=='__main__':
    touch_screen=Touch()
    print('Touchpad created')
    print(touch_screen.scanX())
    

