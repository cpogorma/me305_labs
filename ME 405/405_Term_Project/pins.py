# -*- coding: utf-8 -*-
''' 
@file pins.py
@brief      Contains two list of lists that hold the ccorrect pins for motors, encoders, and touch panels

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/ME%20405/405_Term_Project/
@author     Caleb O'Gorman
@date       03/03/2021'''
import pyb


motors = [
            [pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP), pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP), 
                      pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP), pyb.Timer(3, freq = 20000), 1, 2, 
                      pyb.Pin(pyb.Pin.cpu.B2,pyb.Pin.IN), pyb.Pin(pyb.Pin.cpu.C13)],
          
            [pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP), pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP), 
                      pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP), pyb.Timer(3, freq = 20000), 3, 4,
                      pyb.Pin(pyb.Pin.cpu.B2,pyb.Pin.IN), pyb.Pin(pyb.Pin.cpu.C13)  ]                     
                               ]

encs   = [
            [4, 0xFFFF, pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN), pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)],
            [4, 0xFFFF, pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.IN), pyb.Pin(pyb.Pin.cpu.C7, pyb.Pin.IN)]
                            ]

touch  = [pyb.Pin(pyb.Pin.cpu.A6), pyb.Pin(pyb.Pin.cpu.A0), pyb.Pin(pyb.Pin.cpu.A1),
           pyb.Pin(pyb.Pin.cpu.A7)]