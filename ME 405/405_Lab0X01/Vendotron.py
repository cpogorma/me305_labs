# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 13:27:27 2021

@author: cogor
"""

import time
import keyboard



class vendingMachine:
    '''
    @brief      A finite state machine to control the blinking state
    @details    This class has three states to correspond to each of the three
                settings of blinkage. The state is changed by pressing the blue
                input button
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1
    S1_WAITING              = 1
    
    ## Constant defining State 2
    S2_DISPENSING           = 2
    
    ## Constant defining State 3
    S3_EJECTING             = 3
    
    ## Constant defining State 4
    S4_QUIT                 = 4
    
    ## Blank String defining the last key
    last_key = ''
    
    # List definining the drinks and their prices
    drinks = {'c':['Cuke', 99], 'p':['Popsi', 95], 's':['Spryte', 114], 'd':['Dr. Pupper', 68]}
    # List defining monetary values
    values = {'1': 1, '2':5, '3':10, '4':25, '5':100, '6':500, '7':1000, '8':2000}
    
    def __init__(self):
        '''
        @brief             Creates a vending machine object
        '''
        self.state       = self.S0_INIT #Initial state of the system
        
        self.balance     = 0 #Initial user balance
        
        self.last_input  = time.time() #Initializes the idle time
        
        self.warning = 'Press any key to continue' #Initial warning to be displayed
        
    def run(self):
        '''
        @breif             The FSM that will be ran in a loop
        '''
        curr_time   = time.time() #Updates current time every iterations
        
        # Displays a warning message if enough idle time has passed
        if curr_time - self.last_input > 20: #Gives warning message if ample idle time
            print(self.warning)
            self.last_input = time.time()
        
        elif(self.state == self.S0_INIT): #runs state 0 code
            print('Hello! Press and key to begin')
            while self.last_key == '':  #traps user in loop until they activate vendotron
                pass
            print('Use your number keys to insert money and keyboard to select drink. Press e to eject balance.')
            print('Press m for list of values and z for a list of drinks.')
            self.transitionTo(self.S1_WAITING)
                
        elif(self.state == self.S1_WAITING):   #runs state 1 code
            self.warning = 'Insert mnoney, select drink, or press e to eject.'
            if self.last_key in self.values:               #if the key is a money value
                self.balance += self.values[self.last_key] #add value to balance
                print('Balance is now ${}'.format(self.balance/100))
            elif self.last_key in self.drinks:             #if it is a drink
                self.drink = self.drinks[self.last_key][0]
                self.cost  = self.drinks[self.last_key][1]
                self.transitionTo(self.S2_DISPENSING)
            elif self.last_key == 'e':  #if they eject their balance
                self.transitionTo(self.S3_EJECTING)
            elif self.last_key == 'q':  # if they quit the program
                self.transitionTo(self.S4_QUIT)
            elif self.last_key == 'm':  # if they view money message
                self.moneyList()
            elif self.last_key == 'z':  # if they view drink message
                self.drinkList()
            self.last_key = ''          # resets the last_key
                
        elif(self.state == self.S2_DISPENSING): #Runs state 2 code
            print(('You have selected {} at a price of ${}'.format(self.drink, self.cost/100))) # inform userof drink choice
            self.warning = 'press g to confirm or x to cancel' # sets warning message
            print(self.warning) # prints it once
            while self.last_key != 'g' and self.last_key != 'x': #awaits user response
                pass
            if self.last_key == 'x': # cancels transaction
                print('Transaction canceled.')
                self.transitionTo(self.S1_WAITING)
                return 
            if self.balance > self.cost:  # if the user can afford the drink
                print('here is your {}!'.format(self.drink))
                self.balance -= self.cost # adjust balance
                print('your new balance is {}'.format(self.balance/100)) # print new balance
            else: # if user can't afford
                print('imsufficent funds!') # let them know
            self.transitionTo(self.S1_WAITING) # transition back to main state
    
            
        elif(self.state == self.S3_EJECTING): # runs state 3 code
            print('Ejecting. Here is your money.')
            print(self.eject()) # call eject funciton and give coins
            self.transitionTo(self.S0_INIT) # reset program
            
        elif(self.state == self.S4_QUIT): # runs state 4 code
            print('Quitting. Goodbye!')   # lets them know they're quitting
            keyboard.unhook_all()         # unhooks all keys
            while True:                   # traps user in endless loop
                pass
            
        else:
            #error handling
            pass
        
    def eject(self):
        '''
        @brief determines the coins to eject based on the users balance
        @return money_out a list of coins for the user to recieve
        '''
        vals = list(self.values.values())
        bal = self.balance
        money_out = [0,0,0,0,0,0,0,0] # initialize list of coins
        for i in range(len(vals)):    # iterates through each denomination
            while bal >= vals[-(i+1)]:
                money_out[-(i+1)] += 1
                bal -= vals[-(i+1)]
        return money_out
    
    def moneyList(self):
        '''
        @brief desplays a list of coins/bills and their corresponding key
        '''
        print('Money values: ')
        for i in self.values:
            print('{}: ${}'.format(i, self.values[i]/100))
            
    def drinkList(self):
        '''
        @brief desplays a list of drinks and their corresponding keys
        '''
        print('Drink codes: ')
        for i in self.drinks:
            print('{}: {}'.format(i, self.drinks[i][0]))
        
    def transitionTo(self, new_state):
        '''
        @brief transitions fsm to new state
        @param new_state the new state to be transitioned to
        '''
        self.state = new_state
        self.last_key = ''
        
        
    def kb_cb(self, key):
        '''
        @brief callback function for keyboard interrupt
        '''
        self.last_key   = key.name
        self.last_input = time.time()
         
vendo = vendingMachine()        #creates vendotron object
keyboard.on_release(callback=vendo.kb_cb) #responds to all keys

if __name__ == "__main__":
    while True:  # calls the run function forever. But will trap user in while loop when they press q.
        vendo.run()