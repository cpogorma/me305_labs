'''
@file       think_fast.py
@brief      Runs a game to test a user's reaction time called Think Fast
@details    The program creates a game in which the MCU blinks an LED and the 
            user must attempt to react as quickly as possible by pressing a button
            on the board. The program will then display the user reaction time 
            in ms and allow them to play again. When the user quits the game, 
            the program will display their average reaction time.
            
            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/ME%20405/405_Lab0X02/
@author     Caleb O'Gorman
@date       04/27/2021
'''
from morse import slow_type
import utime
import pyb

## Initialize LED for PWM mode
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

class thinkFast:
    '''
    @brief      A finite state machine to control the reaction time game
    @details    This class has 3 states corresponding to the different sections of
                the game, as well as several methods to make the game run smoothly
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT          = 0
    
    ## Constant defining State 1
    S1_BLINK         = 1
    
    ## Constant defining State 2
    S2_RESPONSE      = 2
    
    def __init__(self):
        '''
        @brief   Creates a thinkFast object.
        '''   
        self.state      = self.S0_INIT #Variable corresponding to the current state
        self.goVar      = 0  #Flag that is set high by pushing/releasing the button
        self.scores = []     #Initialize a list containing the user's scores
        
    def run(self):
        '''
        @brief    Finite state machine that runs the game '''
        if(self.state == self.S0_INIT): #runs state 0 code
            slow_type('Hello!\nRules are simple:\n', 75) #uses fancy video-game style lettering
            slow_type('Press the blue button as soon as you see the grean light flash!\n', 75)
            print('Press button to begin.')
            while self.goVar == 0: #trap user in loop until the button is pressed
                pass
            self.transitionTo(self.S1_BLINK) #transition to state 1
            
        if(self.state == self.S1_BLINK): #runs state 1 code
            t2ch1.pulse_width_percent(0) #turn light off
            start = utime.ticks_ms()     #time stamp the start
            rando = ((pyb.rng() % 9)+1) * 1000 #Random time between 1 and 9 seconds
            while utime.ticks_diff(utime.ticks_ms(),start) < rando: # until the random time is reached, hold
                pass
            t2ch1.pulse_width_percent(100)      #turn light on
            self.on_time = utime.ticks_ms()     #stamp the on time
            self.transitionTo(self.S2_RESPONSE) #transition to state 2
            
        if(self.state == self.S2_RESPONSE): #runs state 2 code
            if self.goVar == 1: #if button is not pressed
                press_time = utime.ticks_ms() #mark press time
                react_time = utime.ticks_diff(press_time, self.on_time) #mark the reaction time
                print('{} ms'.format(react_time)) #display reaction time for the user
                self.scores.append(react_time)    #append to their list of scores
                self.transitionTo(self.S1_BLINK) #transition back to state 1
            if utime.ticks_diff(utime.ticks_ms(), self.on_time) > 1000: # after one second
                t2ch1.pulse_width_percent(0) #turn light off
            
    def transitionTo(self, new_state):
        '''
        @brief transitions fsm to new state
        @param new_state the new state to be transitioned to
        '''
        self.state = new_state
        self.goVar = 0
    
    def buttonPress(self, IRQ_src):
        '''
        @brief   Callback that sets flag goVar high
        @details This ExtInt callback is triggered by a button on the Nucleo and will
                 set a flag high, thus allowing control of multiple facets of the 
                 game.'''
        self.goVar = 1
            
reaction_game = thinkFast() #creates the game object
        

#Pin that reads high when button is pressed and low when button is released
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

#Interrupt handler for the callback triggered by the button press and release
buttonPress = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                         pull=pyb.Pin.PULL_NONE, callback=reaction_game.buttonPress)


if __name__ == "__main__":
    # Try-except is used to account for the user quitting the program
    try:
        while True: #runs the game until cntrl-C is pressed
            reaction_game.run()
            
    except KeyboardInterrupt: #when cntrl-C is pressed
        if len(reaction_game.scores) == 0: #if their is no avg
            slow_type('Boohoo, you never pressed the button.\n', 75)
        else: #display their avg
            avg = sum(reaction_game.scores)/len(reaction_game.scores)
            print('Your average reaction time was {} milleseconds!'.format(avg))
            
