'''
@file       think_fast_pt_B.py
@brief      Runs a game to test a user's reaction time called Think Fast
@details    This program flows exactly the same as think_fast.py except 
            for the fact that the button press interrupt as well as the 
            LED control are both regulated by internal timers, using input
            capture and output compare respectively. The comments included in
            this class serve to differentiate it from the class in 
            think_fast.py. See that file for more in-depth documentation.
            
            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/ME%20405/405_Lab0X02/
@author     Caleb O'Gorman
@date       04/28/2021
'''
from morse import slow_type
import pyb

pinB3 = pyb.Pin (pyb.Pin.cpu.B3) #initializes pin that is connected to the button pin by jumper cable
pinA5 = pyb.Pin (pyb.Pin.cpu.A5) #initializes LED pin
tim2 = pyb.Timer(2, period=0xffff, prescaler=0xffff) #initializes timer 2
#creates timer channel object that does IC on button's voltage and captures on falling edge(ie button pressed)
t2ch2 = tim2.channel(2, pyb.Timer.IC, polarity=tim2.FALLING, pin=pinB3)

class thinkFastPtB:
    '''
    @brief      A finite state machine to control the reaction time game
    @details    This class has 3 states corresponding to the different sections of
                the game, as well as several methods to make the game run smoothly
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT          = 0
    
    ## Constant defining State 1
    S1_BLINK         = 1
    
    ## Constant defining State 2
    S2_RESPONSE      = 2
    
    def __init__(self):
        '''
        @brief             Creates a game object
        '''
        #Variable corresponding to the current state
        self.state      = self.S0_INIT 
        #Flag that is set high by pushing/releasing the button
        self.goVar      = 0
        
        self.scores = []
        
        tim2.counter(0) #sets timer 2's counter to zero
        
        self.light_on = False # Boolean representing if LED is on
        
    def run(self):
        '''
        @brief FSM that runs the game'''
        
        if(self.state == self.S0_INIT): 
            slow_type('Hello!\nRules are simple:\n', 75)
            slow_type('Press the blue button as soon as you see the grean light flash!\n', 75)
            print('Press button to begin.')
            while self.goVar == 0:
                pass
            self.transitionTo(self.S1_BLINK)
            
        if(self.state == self.S1_BLINK):
            self.rando = ((pyb.rng() % 9)+1) * 1000
            tim2.counter(0) #resets timer 2's counter
            t2ch1.compare(self.rando)
            while self.light_on == False:
                pass
            tim2.counter(0) #resets timer's counter
            t2ch1.compare(1000) #resets compare value
            self.transitionTo(self.S2_RESPONSE)
            
        if(self.state == self.S2_RESPONSE):
            if t2ch2.capture() != 0: #when a capture occurs
                print(t2ch2.capture())
                self.scores.append(self.timch2.capture())
                self.transitionTo(self.S1_BLINK)
                t2ch2.capture(0) #resets capture value zero

    def transitionTo(self, new_state):
        '''
        @brief transitions fsm to new state
        @param new_state the new state to be transitioned to
        '''
        self.state = new_state
        self.goVar = 0
        
    def lightOn(self, tim2):
        if self.light_on == False: #if light is off
            self.pinA5.high() #turn it on
        if self.light_on == True: #if light is on
            self.pinA5.low() #turn it off
        self.light_on = not self.light_on #flip boolean
        
    def buttonPress(self, IRQ_src):
        '''
        @brief   Callback that sets flag goVar high
        @details This ExtInt callback is triggered by a button on the Nucleo and will
                 set a flag high, thus allowing control of multiple facets of the 
                 game.
        '''
        self.goVar = 1
        
reaction_game = thinkFastPtB() #creat game object
        
#create timer channel object that will actuate the LED with OC
t2ch1 = tim2.channel(1, pyb.Timer.OC_TOGGLE, callback=reaction_game.lightOn)

pinC13 = pyb.Pin (pyb.Pin.cpu.C13) #Pin that reads low when button is pressed and high when button is released

#Interrupt handler for the callback triggered by the button press and release
buttonPress = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                         pull=pyb.Pin.PULL_NONE, callback=reaction_game.buttonPress)

if __name__ == "__main__":
    try:
        while True:
            reaction_game.run()  
    except KeyboardInterrupt:
        if len(reaction_game.scores) == 0:
            reaction_game.pinA5.low()
            slow_type('Boohoo, you never pressed the button.\n', 75)
        else:
            avg = sum(reaction_game.scores)/len(reaction_game.scores)
            print('Your average reaction time was {} milleseconds!'.format(avg))
            