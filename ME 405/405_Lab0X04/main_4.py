'''
@file       main_4.py
@brief      Contains a script that writes internal and external board temps to a CSV for 8 hours

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/ME%20405/405_Lab0X04/
@author     Caleb O'Gorman
@date       05/11/2021
'''

import pyb
import utime
from mcp9808 import mcp9808

i2c = pyb.I2C(1) #create I2C object
mcp = mcp9808(i2c, 24, 5) #create mcp9808 object at address 24 and memory register 5
adc = pyb.ADCAll(12, 0x70000) #creates ADCALL object with resolution 12
adc.read_core_vref() #Tares the ADCALL reading

with open("temps.csv", "w") as file: #opens file on nucleo in write mode
    while True:
        start = utime.time() #marks start time in seconds
        print('start')
        try: #try-except condition to run until user presses control-C
            while (utime.time() - start) < (480*60): #while loop will run for 8 hours
                #writes a csv with columns of time(s), core temp(C), and ambient temp(C)
                file.write ("{},{},{}\r\n".format((utime.time()-start),adc.read_core_temp(),mcp.celsius()))
                utime.sleep(60) #Sleeps for 60s before next data point
                #prints it to terminal just in case user wants to watch 
                print("{},{},{}\r\n".format((utime.time()-start),adc.read_core_temp(),mcp.celsius()))
            break #breaks out of loop after 8 hours
        except KeyboardInterrupt: #quits if user presses cntrl-c
            print('program quit')