'''
@file       mcp9808.py
@brief      Contains a driver for an Adafruit mcp9808 temperature sensing breakout board
@details    Module contains a method check() to see if the I2C connection has
            been established, a methos celsius() to return the ambient temp in
            celsius and a methos fahrenheit() to return it in degrees F

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/ME%20405/405_Lab0X04/
@author     Caleb O'Gorman
@date       05/11/2021
'''
import pyb
import utime


class mcp9808:
    '''
    @brief a hardware drive for Adafruit breakout board mcp9808
    '''
    def __init__(self, sensor, addr, memaddr):
        '''
        @brief  A constructor to creat an mcp9808 object
        @param sensor The I2C object to be used
        @param addr The I2C address of the sensor
        @param memaddr The pointer which will reference the memory location for temperature
        '''
        self.addr = addr #default is 25 for the mcp9808
        self.memaddr = memaddr #5 for the temperature register
        self.i2c = sensor
        self.i2c.init(pyb.I2C.MASTER, baudrate=115200) #intializes the i2c Nand baudrate
            
    def check(self):
        '''
        @brief Checks if the I2C connection can be established at this specific address
        @return returns T is connection works, false if not
        '''
        return self.i2c.is_ready(self.addr) 
        
    def celsius(self):
        '''
        @brief   Reads ambient temperature from mcp9808
        @return  Ambient temperature in Celsius
        '''
        sens = self.i2c.mem_read(2, self.addr, self.memaddr) #reads 2 bytes from mem address
        byte0 = bin(sens[0])[5:] #splits 1st byte and strips off first 5 characters from string 
        byte1 = bin(sens[1])[2:] #splits 1st byte and strips off first 2 characters from string ('0b')
        while len(byte1) < 8:    #Puts leading zeros back on second byte
            byte1 = '0' + byte1
        byte = byte0 + byte1 #combine bytes into one string of 16 bits
        if float(byte[0]) == 0: #determine wether temp is negative or positive
            sign = 1
        else:
            sign = -1
        temp = 0
        for i in range(len(byte)-1): #iterate through each bit, adding its respective value to the total temp
            temp += float(byte[i+1])*(2**(7-i)) #see mcp9808 data sheet for details
        return sign*temp #return positive or negative temp
    
    def fahrenheit(self): 
        '''
        @brief  Converts the celsius to fahrenheit
        @return Temp in degrees F

        '''
        return self.celsius()*9/5+32
        
    
if __name__ == "__main__":
    #test code for the driver
    i2c = pyb.I2C(1) #creates I2C object
    mcp = mcp9808(i2c, 24, 5) #create mcp object
    start = utime.ticks_ms()  #marks start time
    while True:
        if utime.ticks_diff(utime.ticks_ms(), start) > 1000:
            print(mcp.fahrenheit()) #print temp in fahrenheit every second
            start = utime.ticks_ms()