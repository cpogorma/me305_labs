'''
@file Elevator.py
@brief An imaginary Elevator.
@details This file serves as an example of a finite-state-machine that could control an 
elevator that operates between 2 floors.

There are buttons to call the elevator on each floor.

There is also a proximity sensor on each floor.

@author Caleb O'Gorman
@date   01/24/2021
'''

from random import choice
import time


class TaskElevator:
    '''
    @brief      A finite state machine to control a two floor elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator between two floors
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT               = 0    
    
    ## Constant defining State 1
    S1_STOPPED_AT_SECOND  = 1    
    
    ## Constant defining State 2
    S2_STOPPED_AT_FIRST   = 2    
    
    ## Constant defining State 3
    S3_MOVING_UP          = 3    
    
    ## Constant defining State 4
    S4_MOVING_DOWN        = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING         = 5
     
    def __init__(self, interval, button_1, button_2, second, first, motor):
        '''
        @brief            Creates a TaskWindshield object.
        @param GoButton   An object from class Button representing on/off
        @param LeftLimit  An object from class Button representing the left Limit switch
        @param RightLimit An object from class Button representing the right Limit switch
        @param Motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for the first floor button
        self.button_1 = button_1
        self.button_1.setButtonState(False)
        
        ## The button object used for the second floor button
        self.button_2 = button_2
        self.button_2.setButtonState(False)
        
        ## The button object representing a proximity sensor on the second floor
        self.second = second
        self.second.setButtonState(False)
        
        
        ## The button object representing a proximity sensor on the first floorr
        self.first = first
        self.first.setButtonState(False)
        
        ## The motor object moving the elevator
        self.motor = motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    Will check state depending repeatedly at the interval specified
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S4_MOVING_DOWN)
                self.motor.Down()
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S1_STOPPED_AT_SECOND):
                # Run State 1 Code
                
                if(self.runs*self.interval > 10):  # if our task runs for 10 seconds, transition
                    self.transitionTo(self.S5_DO_NOTHING)
                
                if(self.button_1.getButtonState()):
                    self.transitionTo(self.S4_MOVING_DOWN)
                    self.motor.Down()
                    self.second.setButtonState(False)
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_STOPPED_AT_FIRST):
                # Run State 2 Code
                if(self.button_2.getButtonState()):
                    self.transitionTo(self.S3_MOVING_UP)
                    self.motor.Up()
                    self.first.setButtonState(False)
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S3_MOVING_UP):
                # Transition to state 1 if the second floor proximity sensor is active
                if(self.second.getButtonState()):
                    self.transitionTo(self.S1_STOPPED_AT_SECOND)
                    self.motor.Stop()
                    self.button_2.setButtonState(False)
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S4_MOVING_DOWN):
                # Run State 4 Code
                if(self.first.getButtonState()):
                    self.transitionTo(self.S2_STOPPED_AT_FIRST)
                    self.motor.Stop()
                    self.button_1.setButtonState(False)
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
                
            elif(self.state == self.S5_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary rider to call the elevator to either floor. As of right now
                this class is implemented using "pseudo-hardware". The class is
                also used for buttons representing proximity sensors that
                determine when the elevator has reached the first or second floor.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])
    
    
    def setButtonState(self, state):
        '''
        @brief      Sets the button state.
        @details    This method allows clearing the buttons upon start-up
                    and after certain transitions, such as clearing the floor
                    buttons when the elevator has reached the desired floor
        -------
        None.
        '''
        self.state = state

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the motor and elevator upwards
        '''
        print('Elevator moving Up')
    
    def Down(self):
        '''
        @brief Moves the motor and elevator downwards
        '''
        print('Elevator moving Down')
    
    def Stop(self):
        '''
        @brief Stops the motor and elevator
        '''
        print('ELevator Stopped')


#Initialize objects of the Button and "Motor Driver" type
button_1 = Button('PB6')
button_2 = Button('PB7')
first = Button('PB8')
second = Button('PB9')
motor = MotorDriver()


# Creating a task object using the button and motor objects above
task1 = TaskElevator(0.1, button_1, button_2, first, second, motor)
task2 = TaskElevator(0.1, button_1, button_2, first, second, motor)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
     task1.run()
     task2.run()
#    task3.run()





















