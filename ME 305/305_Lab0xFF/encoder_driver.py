# -*- coding: utf-8 -*-
''' @file encoder_driver.py
@brief      Contains an encoder driver class to create encoder objects and an
            encoder_task.

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/Lab0xFF/
@author     Caleb O'Gorman
@date       03/03/2021'''

import pyb
import utime
import shares

class encoderDriver:
    '''This class implements an encoder driver for the
    ME405 board. '''
    
    def __init__(self, timer_number, period, first_pin, second_pin, update_period):
        ''' Creates a encoder driver by initializing GPIO
        pins and zeroing the initial position
        @param timer_number   The number of the timer that will be used
        @param period         The timer's period
        @param first_pin      The number of the pin that will be used for channel 1
        @param secod_pin      The numberof the pin that will be used for channel 1
        @param update_period  The period at which the update function is called. '''
        self.timer      = pyb.Timer(timer_number, precscaler = 0, period=period)
        self.ch1        = self.timer.channel(1, pyb.Timer.ENC_A, pin=first_pin)
        self.ch2        = self.timer.channel(2, pyb.Timer.ENC_B, pin=second_pin)
        self.timer.counter(0)
        self.period     = period
        self.position   = 0
        self.curr_count = 0
        self.rads       = 0
        
    def update(self):
        '''
        This method updates the encoders position and returns it as in radians. 
        It needs to be called at a specified frequency to ensure no ticks are lost. 
        See @ref analysis ["this documentaion"] for more details.'''
        delta = self.get_delta()
        self.position += delta
        self.rads      = self.position/28
        return self.rads
        
    def set_position(self, desired):
        '''
        This metod sets the encoders position to a desired value. It will 
        typically be used for zeroing the encoder
        @param desired The value to which the position will be set
        '''
        self.position = desired
        
    def get_delta(self):
        '''
        The method compares the previous encoder count to the current encoder 
        count in order to get the change in position. It is built to correct
        overflow effor to ensure the encoder will recognize a change in direction.
        It returns the delta        '''
        last_count = self.curr_count                # stores the last count
        self.curr_count = self.timer.counter()      # gets the current count
        delta = self.curr_count - last_count        # calculates the diff
        if (-self.period/2 < delta < self.period/2):
            pass                       # These conditionals determined
        elif (delta > self.period/2):  # whether or not the motor has changed
            delta -= self.period       # direction and than adjusts the delta
        elif (delta < -self.period/2): # accordingly
            delta += self.period
        return delta
    
    def get_speed(self):
        '''
        This method, given the update period, will compare the last two counts
        to return the motor's speed.'''
        last_position  = self.rads          # Stores the last position
        curr_position  = self.update()      # Gets the new position
        delta_position = last_position - curr_position
        return delta_position/self.update_period # Returns the angular speed
    

def encoder_task(enc):
    '''
    Calls the update function of the encoder task at a specified update period
    @param enc the encoder object unto which the task will act
    '''
    reset = utime.ticks_ms()
    curr_time = reset
    while True:
        curr_time = utime.ticks_ms
        if curr_time - reset > enc.update_period:
            shares.position = enc.update()
            