# -*- coding: utf-8 -*-
''' @file main.py
@brief      File containing the main task that will run for the term project

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/Lab0xFF/
@author     Caleb O'Gorman
@date       03/14/2021'''
from motor_driver import motorDriver
from encoder_driver import encoderDriver, encoder_task
from closed_loop import closedLoop
from task_ciper import taskCipher
from pins import motors, encs
from shares import position
import utime

moe    = motorDriver(motors[0][0], motors[0][1], motors[0][2], motors[0][3], motors[0][4], motors[0][5])
enc    = encoderDriver(encs[0][0], encs[0][1], encs[0][2], encs[0][3])
c_loop = closedLoop(30)
cipher = taskCipher(enc)

period = .357 # The period at which the main task will run

class Controller:
    '''
    Class for the main controller that will run the entire system
    '''
    def __init__(self, moe, enc, c_loop):
        '''Creates a controller that will manage the multitasking between motor,
        encoder, and control system
        @param moe    The motor object
        @param enc    The encoder object
        @param c_loop The closed loop object'''
        self.motor   = moe
        self.encoder = enc
        self.c_loop  = c_loop
        
    def run(self):
        '''The main task that runs which will pass the measured speed through the
        control system and set the motor's effort level'''
        self.enc.update()
        speed  = self.encoder.get_speed()
        effort = self.c_loop.run(speed)
        self.motor.set_duty(effort)
    
    def set_speed_des(self, desired_speed):
        ''' This method is used for setting a reference velocity for the closed
        loop controller. It can be used for testing or for reference control'''
        self.c_loop.speed_des = desired_speed
        
        
controller = Controller(moe, enc, c_loop)        
        
if __name__ == '__main__':
    reset = utime.ticks_ms()
    while True: 
        cipher.run()
        if utime.ticks_diff(utime.ticks_ms, reset) > period:
            Controller.run()
            reset = utime.ticks_ms()