# -*- coding: utf-8 -*-
'''
@file       task_cipher.py
@brief      Contains a uart class that runs on the pyboard

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/Lab0xFF/
@author     Caleb O'Gorman
@date       02/24/2021
'''

from pyb import UART
import utime
from array import array

class taskCipher:
    '''
    @brief a finite state machine class that controls data collection for a specified function
    '''
    
    S0_WAITING           = 0 ## Constant defining State 0 - Waiting for char
    
    S1_COLLECT_DATA      = 1 ## Constant defining State 1
    
    S2_SEND_DATA         = 2 ## Constant defining State 3
    
    def __init__(self, enc): # interval in ms
        '''
        @brief constructor for objects of the data collection class
        @param interval the time interval in ms in at which data will be sampled
        '''
        self.state    = self.S0_WAITING # will start at state 0
        self.values   = array('f', [0]) # intialize values array
        self.times    = array('f', [0]) # intialize array to hold times
        self.enc      = enc
        self.myuart   = UART(2)
        
    def run(self):
        '''
        @brief method that runs the finite state machine
        '''
        if self.state == self.S0_WAITING: # state 0 waits for user to send a 'g'
            value = '' # intialize variable that will correspond to the 'g'
            while value == 0: # untik user sends a g
                if self.myuart.any() != 0: # if user sends something to UART terminal
                    value = self.myuart.readchar().decode() # than read it and set it to val
            if value == 'g':
                self.state  = self.S1_COLLECT_DATA  #transition states
            if value == 'p':
                self.myuart.write(self.enc.position)
            if value == 'd':
                self.myuart.write(self.enc.get_delta)
            if value == 'z':
                self.enc.set_position(0)
            value = 0
                
                
        if self.state   == self.S1_COLLECT_DATA: # state 1 calls function to collect data
            self.collect_data()
            self.state  = self.S2_SEND_DATA      # transition states
            
        if self.state == self.S2_SEND_DATA:      # state 2 sends data back to the serial port
            self.myuart.write('{:}\r\n'.format(len(self.times))) # 1st sends length of arrays
            for n in range(len(self.times)):     # for the length of the array, send data and times as csv
                self.myuart.write('{:},{:}\r\n'.format(self.values[n], self.times[n])) 
            self.state = self.S0_WAITING
                
    def collect_data(self):
        '''
        @brief method that will evaluate the function and append to array
        @details this method will sample time values at every interval until 
                 being instructed to stop by recieving an ascii s or running for 
                 30 seconds. Every time value that is collected will be fed into
                 a function and evaluated as f(t). The times and values will be 
                 appended to their corresponding arrays.
        '''

        start_time = utime.ticks_ms()
        curr_time  = start_time
        value = ''                        # variable to hold the next character sent
        while value != 's' and curr_time-start_time < 30000: # until user sends s or time reaches 30 s
            curr_time = utime.ticks_ms
            if self.myuart.any() != 0:       # if something in UART terminal
                value = self.myuart.readchar().decode() # read the character
                
    
