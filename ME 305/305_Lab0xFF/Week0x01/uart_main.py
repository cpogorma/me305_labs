# -*- coding: utf-8 -*-
'''
@file       uart_main.py
@brief      Contains a uart class that runs on the pyboard

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/Lab0xFF/Week0x01/
@author     Caleb O'Gorman
@date       02/24/2021
'''

from pyb import UART
import utime
from array import array
from math import exp, sin, pi

myuart = UART(2) # initialize UART

class dataCollection:
    '''
    @brief a finite state machine class that controls data collection for a specified function
    '''
    
    S0_WAITING           = 0 ## Constant defining State 0 - Waiting for char
    
    S1_COLLECT_DATA      = 1 ## Constant defining State 1
    
    S2_SEND_DATA         = 2 ## Constant defining State 3
    
    def __init__(self, interval = 500): # interval in ms
        '''
        @brief constructor for objects of the data collection class
        @param interval the time interval in ms in at which data will be sampled
        '''
        self.state    = self.S0_WAITING # will start at state 0
        self.interval = interval        # interval
        self.values   = array('f', [0]) # intialize values array
        self.times    = array('f', [0]) # intialize array to hold times
        
    def run(self):
        '''
        @brief method that runs the finite state machine
        '''
        if self.state == self.S0_WAITING: # state 0 waits for user to send a 'g'
            val = 0 # intialize variable that will correspond to the 'g'
            while val  != 103: # untik user sends a g
                if myuart.any() != 0: # if user sends something to UART terminal
                    val = myuart.readchar() # than read it and set it to val
            self.state  = self.S1_COLLECT_DATA  #transition states
                
        if self.state   == self.S1_COLLECT_DATA: # state 1 calls function to collect data
            self.collect_data()
            self.state  = self.S2_SEND_DATA      # transition states
            
        if self.state == self.S2_SEND_DATA:      # state 2 sends data back to the serial port
            myuart.write('{:}\r\n'.format(len(self.times))) # 1st sends length of arrays
            for n in range(len(self.times)):     # for the length of the array, send data and times as csv
                myuart.write('{:},{:}\r\n'.format(self.values[n], self.times[n])) 
            while True: # trap code in a while loop until nucleo is reset
                pass
                
    def collect_data(self):
        '''
        @brief method that will evaluate the function and append to array
        @details this method will sample time values at every interval until 
                 being instructed to stop by recieving an ascii s or running for 
                 30 seconds. Every time value that is collected will be fed into
                 a function and evaluated as f(t). The times and values will be 
                 appended to their corresponding arrays.
        '''
        start = utime.ticks_ms() # start time for function
        reset = utime.ticks_ms() # counter reset for interval modulation
        length = (30000/self.interval) # designated length of array
        val = 0                        # variable to hold the next character sent
        while val != 115 and len(self.times) < length: # until user sends s or time reaches 30 s
            curr = utime.ticks_ms()     # stamps current time
            if myuart.any() != 0:       # if something in UART terminal
                val = myuart.readchar() # read the character
                
            if curr > (reset + self.interval):         # if it is time to sample data
                self.times.append((curr - start)/1000) # append current time
                self.values.append(exp(-self.times[-1]/10)*sin(2*pi/3*self.times[-1])) # evaluate f(t)
                reset = curr                           # reset counter
        
uart_task = dataCollection(10) # creates data collection object and passes in interval

for N in range(10000000):      # effectively while True
    uart_task.run()            #Runs task