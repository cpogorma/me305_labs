# -*- coding: utf-8 -*-
'''
@file       ascii_frontend.py
@brief      Contains a front-end UI for communicating with the pyboard

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/Lab0xFF/Week0x01/
@author     Caleb O'Gorman
@date       02/24/2021
'''
import serial, keyboard

from array import array
from matplotlib import pyplot

last_key = None # Variable to store the value of the last key for keyboard interrupt
run = True      # variable that tells if data is still being collected

def kb_cb(key): 
    '''
    @brief callback function for keyboard interrupt
    @param key the last key that was pressed
    '''
    global last_key     # global variable for last key that was pressed
    last_key = key.name # sets it equal to the name of the last key

ser = serial.Serial (port='COM3', baudrate = 115200, timeout=1) # inititalize serial port
keyboard.on_release_key("S", callback=kb_cb)  # initialize keyboard interrupt for s
keyboard.on_release_key("G", callback=kb_cb)  # initialize keyboard interrupt for g

runs = 0 # varaible that helps determine if we recieve length or data

print('Press g to begin data collection and s to terminate')
while run == True:           # while told to run
    if last_key is not None: # if key has been pressed
        ser.write((str(last_key).lower()).encode('ascii')) # writes the ascii character to UART
        last_key = None      # resets last key
            
    if ser.in_waiting != 0: # if UART is sending something
        if runs == 0:       # UART sends the length of the arrays first
            length = int((ser.readline().decode('ascii')).strip()) # sends length
            times  = array('f', [0]) # intitialize times array
            values = array('f', [0]) # intialize values array
            runs = 1                 # tells code that we well recieve values next
        if runs == 1:                # recieving values
            # opens a text file on the desktop to write csv. opened in write/read +
            file = open(r"C:\Users\cogor\Desktop\Lab0xFF_Data.txt", "w+") 
            file.truncate(0)           # deletes current contents of file
            while len(times) < length: # appends values until it reaches the desired length
                csv = ser.readline().decode('ascii') # catches values as CSVs
                file.write('{:}\n'.format(csv))# writes them to the txt file
                stripped = csv.strip()         # strips the csv of special characters
                split = stripped.split(',')    # splits the csv at the comma
                values.append(float(split[0])) # appends the value to the array
                times.append(float(split[1]))  # appends the time to the array
            run = False
                
file.close()                 # close txt file
keyboard.unhook_all()        # unhook all keys
ser.close()                  # close the serial port

pyplot.figure(num=1)         # create a new figure
pyplot.plot(times, values)   # plots the values and times
pyplot.xlabel('Time [s]')    # labels x axis
pyplot.ylabel('Data')        # labels y axis
pyplot.title('Data vs Time') # titles plot
