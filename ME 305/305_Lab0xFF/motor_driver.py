# -*- coding: utf-8 -*-

''' @file motor_driver.py
@brief      Contains a motor driver class to create motor objects

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/Lab0xFF/
@author     Caleb O'Gorman
@date       03/10/2021'''

import pyb
from pins import motors


class motorDriver:
    '''This class implements a motor driver for the
    ME405 board. '''

    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer, tch1, tch2):

            
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on
        IN1_pin and IN2_pin. '''
         
        print ('Creating a motor driver')
        self.nSLEEP_pin = nSLEEP_pin
        self.timer      = timer
        self.ch1        = timer.channel(tch1, pyb.Timer.PWM, pin=IN1_pin)
        self.ch2        = timer.channel(tch2, pyb.Timer.PWM, pin=IN2_pin)


    def enable (self):
        self.nSLEEP_pin.high()
        
    def disable (self):
        self.nSLEEP_pin.low()
        
    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        if duty == 0:
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(0)
        elif duty < 0:
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(abs(duty))
        elif duty > 0:    
            self.ch1.pulse_width_percent(duty)
            self.ch2.pulse_width_percent(0)
        else:
            pass
            
if __name__ == '__main__':
# Adjust the following code to write a test program for your motor class. Any
# code within the if __name__ == '__main__' block will only run when the
# script is executed as a standalone program. If the script is imported as
# a module the code block will not run.
# Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP = None;
    pin_IN1 = None;
    pin_IN2 = None;
    
    # Create the timer object used for PWM generation
    tim = None;
    
    # Create a motor object passing in the pins and timer
    moe = motorDriver(motors[0][0], motors[0][1], motors[0][2], motors[0][3], motors[0][4], motors[0][5])
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 10 percent
    moe.set_duty(40)
