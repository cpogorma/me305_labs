''' @file closed_loop.py
@brief      Closed loop controller class that integrates proportional control
            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/Lab0xFF/
@author     Caleb O'Gorman
@date       03/10/2021'''

class closedLoop:
    '''This class implements a closed loop control system for the
    ME405 board. '''
    def __init__(self, initial_kp, effort_limit = 100):
        '''
        Conrstucts a closed loop controller that uses proportional control
        @param initial_kp The initial proportional gain that will be tuned manually
        @effort_limit The maximum value the motor can run at
        '''
        self.effort_limit = effort_limit
        self.kp  = initial_kp
        self.speed_des = 0
        
    def run(self, speed_actual):
        '''
        Returns the new effort level % for the motor given the desired speed and
        actual speed.
        @param speed_des The current desired speed
        '''
        return self.kp(self.speed_des - speed_actual)
        
    def get_Kp(self):
        '''
        Returns the current value of the proportional gain
        '''
        return self.kp
    
    def set_Kp(self, new_kp):
        '''
        Set a new proportional gain value
        @param new_kp The new value to which to set kp
        '''
        self.kp = new_kp
        
        