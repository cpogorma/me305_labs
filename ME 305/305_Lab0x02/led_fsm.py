'''
@file       led_fsm.py
@brief      Blinks an LED in three different patterns
@details    The program allows a user to cycle through three different settings 
            for blinking an LED on the nucleo. The options are a sign wave with 
            a period of one second, a triangle wave with a one second period, 
            or a sine with with a ten second period. The brightness variation
            (blinking) is achieved by adjusting the duty cycle and keeping the 
            timer frequency reasonably high.
            
            It contains built-in error management for cases in which the user never
            pushes the button or holds the button down. 
            
            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/Lab0x02/
@author     Caleb O'Gorman
@date       01/26/2021
'''
import pyb
import utime
import math

# Configures a timer on channel 2, pin A5 that will be used for the PWM
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

print('Hello there! Would you like to see me blink my led? Push on the blue button to cycle between different settings.')

class taskBlinking:
    '''
    @brief      A finite state machine to control the blinking state
    @details    This class has three states to correspond to each of the three
                settings of blinkage. The state is changed by pressing the blue
                input button
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT               = 0
    
    ## Constant defining State 1
    S1_SQUARE_WAVE        = 1
    
    ## Constant defining State 2
    S2_SINE_WAVE          = 2
    
    ## Constant defining State 3
    S3_TRIANGLE_WAVE      = 3
    
    def __init__(self):
        '''
        @brief             Creates a taskBlinking object.
        @param state       An object from class Button representing on/off
        @param start_time  A timestamp marking the start of the program (ms)
        @param last_press  The last time the button was pressed (ms)
        @param warning     The last time a warning was issued (ms)
        @param reset       Reset the count at the end of the 1s period
        @param count       Counts up from the last reset (s)
        @param sq          A binary used to control the sqare wave
        '''
        self.state       = self.S0_INIT 
        
        self.start_time  = utime.ticks_ms() 
        
        self.last_press  = self.warning = self.reset = self.start_time
        
        self.count       = self.sq = 0
        
    def run(self):
        '''
        @param curr_time    Marks the current time (ms)
        @param run_time     Counts up from start_time (ms)
        @param since_press  Counts up from the last_press (s)
        '''
        self.curr_time   = utime.ticks_ms()
        
        self.run_time    = utime.ticks_diff(self.curr_time, self.start_time)
        
        self.since_press = float(utime.ticks_diff(self.curr_time, self.last_press)) / 1000
        
        self.count       = float((utime.ticks_diff(self.curr_time, self.reset))) / 1000
        
        # Resets count if it reaches 1 second period
        if self.count >= 1:
            self.reset = utime.ticks_ms()
            
        if(self.state == self.S0_INIT): 
            #Runs state 0 code
            self.nextState = self.S1_SQUARE_WAVE
                
        if(self.state == self.S1_SQUARE_WAVE):
            #Runs state 1 code
            self.nextState = self.S2_SINE_WAVE
            # Will return a zero for the first half of the one second period, and a one after
            if self.count < .5:
                self.sq = 0
            else:
                self.sq = 1
            # Configures the led at a duty cycle dependent on the binary sq (in percentage)
            t2ch1.pulse_width_percent(self.sq * 100)
                
        if(self.state == self.S2_SINE_WAVE):
            #Runs state 2 code
            self.nextState = self.S3_TRIANGLE_WAVE
            # Converts time to corresponding radians
            self.rads      = self.since_press * (math.pi)/5
            # Configures the sine wave above the x axis and scales amplitude to 50
            t2ch1.pulse_width_percent((math.sin(self.rads) + 1) * 50)
                                           
        if(self.state == self.S3_TRIANGLE_WAVE):
            #Runs state 3 code
            self.nextState = self.S1_SQUARE_WAVE
            # Triangle wave, thanks to count
            t2ch1.pulse_width_percent(self.count * 100)
            
        else:
            #error handling
            pass
        
        
        if(utime.ticks_diff(self.curr_time, self.warning) > 15000):
            # Runs warning procedure if user hasnt interacted in 15 seconds
            self.warning = self.curr_time
            if(self.state == self.S0_INIT):
                # Runs if user has never pressed button
                    print('Press the blue button to begin the light show!')
                
            else:
                #Runs if user has pressed button at least once
                    print('Push the blue button to continue to next setting.')

            
    def transitionTo(self, IRQ_src):
        '''
        @brief   Callback that updates variables defining the next state and timestamps
        @details This ExtInt callback is triggered by a button on the Nucleo and will
                 transition the FSM to the next state in the sequence. It also updates 
                 the timestamps so the the blinking patterns will begin at the 
                 start location on their waveform.
        '''
        # Transitions state to next state in sequence
        self.state      =  self.nextState
        self.last_press = utime.ticks_ms()
        self.reset      = utime.ticks_ms()
        # Alerts user of the new setting
        if(self.state == self.S1_SQUARE_WAVE):
            print('Pulsing a square wave!')
        if(self.state == self.S2_SINE_WAVE):
            print('Pulsing a sine wave!')
        if(self.state == self.S3_TRIANGLE_WAVE):
            print('Pulsing a triangle wave!')

# Configure pin object for use with interrupt callback
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

# Constructs an object of the taskBlinking class
task1 = taskBlinking()

#Interrupt handler for the callback triggered by the button press
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=task1.transitionTo)

#Runs Task over and over, assuming main file
if __name__ == "__main__":
    for N in range(10000000): # effectively while True
        task1.run()