'''
@file       fibonacci.py
@brief      Evaluates the Fibonacci sequence to a specific index
@details    The program will ask for a user input corresponding to the index 
            of their desired Fibonacci number. It will then compute the sequence
            up to that point and return the desired index. 
            
            It contains built-in error management for non-integer and 
            overly-large indices
'''

def fib (idx):
    '''
    @brief       Calculates the Fibonacci number at a specified index.
    @param idx   An integer between 0 and 100,000 specifying the index of the desired Fibonacci number.
    @param fibo  A list containing all of Fibonacci numbers up to desired index. Appended
                 with subsequent indices as they are calculated.
    @return      The Fibonacci number at the desired index.
    '''
    fibo = [0,1]    
    for n in range(idx):
        fibo.append(fibo[n] + fibo[n+1])
        ## For some reason, indices of the list contents are one ahead of the fibonacci sequence;
        ## therefore the desired fibonacci number must be referenced with [idx-1]
    return fibo[idx-1]

if __name__ == '__main__':
    while True:
        idx = (input("Please input the index of the desired Fibonacci number: "))
        ## Test if user has inputted a number
        try:
            ## Converts to integer format
            idx = int(idx)
            ## Ensures that the inputted integer is less than 100,000 at which point the code
            ## can no longer run with a reasonable runtime
            if idx < 100000:
                fib(idx)
                print ('Fibonacci number at '
                       'index {:} is {:}.'.format(idx,fib(idx)))
            else:
                print('Index cannot exceed 100,000')
                pass
            ## Promts user to input a number if they had inputted a string
        except ValueError:
            print('Input must be an integer')
            
     