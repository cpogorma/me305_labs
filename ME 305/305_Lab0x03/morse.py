# -*- coding: utf-8 -*-
'''
@file       morse.py
@brief      Contains dictionaries and functions for the simon_says game

            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/Lab0x03/
@author     Caleb O'Gorman
@date       02/17/2021
'''
import urandom, utime, pyb

code = {' ': ' ',     # Dictionary that contains all 23 letters, 9 numbers, and common symbols as morse code
        "'": '.----.', 
        '(': '-.--.-', 
        ')': '-.--.-', 
        ',': '--..--', 
        '-': '-....-', 
        '.': '.-.-.-', 
        '/': '-..-.', 
        '0': '-----', 
        '1': '.----', 
        '2': '..---', 
        '3': '...--', 
        '4': '....-', 
        '5': '.....', 
        '6': '-....', 
        '7': '--...', 
        '8': '---..', 
        '9': '----.', 
        ':': '---...', 
        ';': '-.-.-.', 
        '?': '..--..', 
        'A': '.-', 
        'B': '-...', 
        'C': '-.-.', 
        'D': '-..', 
        'E': '.', 
        'F': '..-.', 
        'G': '--.', 
        'H': '....', 
        'I': '..', 
        'J': '.---', 
        'K': '-.-', 
        'L': '.-..', 
        'M': '--', 
        'N': '-.', 
        'O': '---', 
        'P': '.--.', 
        'Q': '--.-', 
        'R': '.-.', 
        'S': '...', 
        'T': '-', 
        'U': '..-', 
        'V': '...-', 
        'W': '.--', 
        'X': '-..-', 
        'Y': '-.--', 
        'Z': '--..', 
        '_': '..--.-'}

#Dictionary containing the integer equivalent of each difficulty string
diff = {'practice': 0, 'easy': 1, 'hard': 2, 'splash': 3}

#Word bank list for practice difficulty
practice = ['aa', 'bb', 'ee', 'ii']
#Word bank list for easy difficulty
easy = ['cat', 'dog', 'jim', 'hog', 'hat', 'sum', 'age', 'aft']
#Word bank list for hard difficulty
hard = ['spectacular', 'serendipity', 'interstellar', 'scrumptious', 'diggity', 'dollup']
#Word bank list for secret easter-egg difficulty containing roommate inside jokes
splash = ['nick', 'dion', 'juan', 'anna', 'seeganassy', 'bryden', 'autumn', 'jason']
#A list of all the list
words = [practice, easy, hard, splash]

def genWord(difficulty):
    '''
    @brief  Reuturns a random word from the word bank corresponding to the selected difficulty
    @param difficulty integer corresponding to the selected difficulty
    @return Returns a random word for the correct word bank 
    '''
    word_list = words[difficulty]
    rng = len(word_list)   #Range for random integer
    rando = (pyb.rng() % rng) #Random integer
    return word_list[rando] 
   
def to_morse_code(letter):
    '''
    @Brief   returns letter as a morse sequence plus a morse space
    @parm    letter Letter from the word defined in genWord and passed in from the game object.
    @returns A string of morse code corresponding to the passed in letter
    '''
    return (code[letter.upper()] + '|')

def slow_type(string, typing_speed = 50):
    '''
    @Brief types a string to resemble human typing
    @param string The string to be typed
    @param typing_speed the words per minute
    '''
    x = 0
    start = utime.ticks_ms()
    #next time a letter will print
    next_t = (pyb.rng() % 9)/typing_speed*1000 + start
    while x < len(string):
        if utime.ticks_ms() > next_t:
            print(string[x], end = "")
            start = utime.ticks_ms()
            #next time a letter will print
            next_t = (pyb.rng() % 9)/typing_speed*1000 + start
            x += 1
            
          