# -*- coding: utf-8 -*-
'''
@file       simon_taps.py
@brief      Runs a simon says game in Morse code
@details    The program creates a game in which the MCU blinks an LED to spell a 
            random the word and the user must push a button to repeat the word back.
            It begins by starting with the first letter of the word and then, assuming 
            the user repeats the patter correctly, continues to the next letter 
            until the word is fully spelt.
        
            
            See code here: https://bitbucket.org/cpogorma/me305_labs/src/master/Lab0x03/
@author     Caleb O'Gorman
@date       02/17/2021
'''
from morse import *
from morse import slow_type
import utime, pyb

## Initialize LED for PWM
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

## Game SETUP
# Prompt user for difficulty until they give an acceptable response
# Difficulty string is mached to a corresponding int as defined in diff dictionar in morse.py
slow_type('Choose your difficulty. Enter "practice," "easy," or "hard": ')
user_diff  = input()
# Difficulty string is mached to a corresponding int as defined in diff dictionar in morse.py
difficulty = diff[user_diff] #integer
while (difficulty != 0 and difficulty != 1 and difficulty != 2 and difficulty != 3):
    difficulty = diff[input('Please enter "easy" or "hard": ')]
      
# Ask user if they would like to see the rules until they provide an acceptable response    
slow_type('Hear rules? "y" or "n": ')
rules = input()
while (rules != 'y' and rules != 'n'):
    rules = input('Please enter "y" to hear rules or "n" to skip: ')
  
# Display rules if user would like to see them    
if rules == 'y':    
# Welcome message
    slow_type('Welcome to Simon Taps, the new-age morse code simulator! \n Rules:\n', 75) 
    slow_type('1. I will tap a message in morse code \n', 75)
    slow_type('2. Watch the LED, then repeat the pattern back with the blue button \n', 75)
    slow_type('3. You will also use the button to control the game \n', 75)
    slow_type('4. Most importantly: ', 75)
    slow_type('WIN', 5)
    # skip severl lines after directions for clarity
    print('\n\n\n\n')


class simonSays:
    '''
    @brief      A finite state machine to control the simon says game
    @details    This class has 8 states corresponding to the different sections of
                the game, as well as several methods to make the game run smoothly
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1
    S1_GENERATE_PATTERN     = 1
    
    ## Constant defining State 2
    S2_SIMON_TAPPING        = 2
    
    ## Constant defining State 3
    S3_USER_TAPPING         = 3
    
    ## Constant defining State 4
    S4_WAITING              = 4
    
    ## Constant defining State 5
    S5_FAILURE              = 5
    
    ## Constant defining State 6
    S6_ROUND_WIN            = 6
    
    ## Constant defining State 7
    S7_GAME_WIN             = 7
    
    def __init__(self, difficulty, unit = 300, tolerance = 600):
        '''
        @brief             Creates a simon_says object.
        @param difficulty  A user defined variable that sets the games difficulty
        @param unit        A value [in ms] that determines the time length of one "dot"
        @param tolerance   A value [in ms] that determines the allowable error of the user's input'
        '''
        #Variable corresponding to the current state
        self.state      = self.S0_INIT 
        #Flag that is set high by pushing/releasing the button
        self.goVar      = 0
        self.difficulty = difficulty
        self.unit       = unit
        self.tolerance  = tolerance
        
    def run(self):
        '''
        @brief        Runs the game object of the simon_says class

        '''
        if(self.state == self.S0_INIT): 
            #Runs state 0 code
            #Varibale that hold the user's current level (1st letter: level = 1)
            self.level = 1
            ## Housekeeping ##
            self.nextState = self.S1_GENERATE_PATTERN
            # Waits for users response
            self.wait()
                
        if(self.state == self.S1_GENERATE_PATTERN):
            #Runs state 1 code - picks word and converts to morse
            #Gnerates random word from bank defined in morse.py
            self.word = genWord(difficulty)
            #Tells user which word they will spell
            slow_type('You will be spelling {}. \n'.format(self.word.upper()))
            #Defines an empty list that will be filled with morse entries for each letter
            self.morse_code = []
            for x in self.word:
                self.morse_code.append(to_morse_code(x)) #Fills morse_code list
            ## Housekeeping ##
            self.state = self.S2_SIMON_TAPPING
            
            
        if(self.state == self.S2_SIMON_TAPPING):
            #Runs state 2 code - Blink Pattern
            self.round = '' #Empty string corresponding to the letters that will be spelled
            self.toTimeSequence()
            slow_type('You are on level {}, {}: \n'.format(self.level, self.round.upper()))
            print(self.morse_code[0:self.level])
            slow_type('Watch as I demonstrate\n', 75)
            self.countdown()
            ## Housekeeping ##
            self.blink()
            self.state = self.S3_USER_TAPPING
                        
        if(self.state == self.S3_USER_TAPPING):
            #Runs state 3 code
            slow_type('Ok now repeat that on go.\n')
            self.countdown()
            self.listen()

        if(self.state == self.S4_WAITING):
            #Runs state 4 code - wait until button press
            while self.goVar != 1:
                pass
            self.transition()
                
        if(self.state == self.S5_FAILURE):
            #Runs state 5 code - user messed up
            slow_type('You biffed it! Press button to restart, or Cntrl+C to quit.\n')
            self.goVar = 0
            self.nextState = self.S0_INIT
            self.wait()
                
        if(self.state == self.S6_ROUND_WIN):
            #Runs state 6 code - user wins round
            slow_type('Wow, great job. You passed level {}.\n'.format(self.level))
            celebrate((self.level*2), .5)
            if self.level == len(self.word):
                self.nextState = self.S7_GAME_WIN
            else:
                self.nextState = self.S2_SIMON_TAPPING
                #Moves user to next level
                self.level += 1
            print('Press button for the next round')
            self.wait()
            
        if(self.state == self.S7_GAME_WIN):
            #Runs state 7 code - user spells entire word
            slow_type('CONGRATULATIONS! You passed all {} levels and spelled {}.\n'
                       .format(self.level, self.word.upper()))
            slow_type ('You are the champion of the {} difficulty.\n'.format(self.user_diff))
            celebrate()
            slow_type('Press button to play again or cntrl C quit.\n')
            self.nextState = self.S0_INIT
            self.wait()
                        
        else:
            pass
                        
    def wait(self):
        '''
        @brief Moves user to the waiting state and sets button flag to zero
        '''
        self.goVar = 0
        self.state = self.S4_WAITING
        
    def transition(self):
        self.goVar = 0
        self.state = self.nextState
        
    def blink(self):
        '''
        @brief Blinks the led in a seuence defined by a list of on and off times

        '''
        #index
        i = 0
        start_time = utime.ticks_ms() #timestamp for start of function [ms]
        while i < (len(self.pattern)): # blinks led
            curr_time = utime.ticks_ms() #current time [ms]
            if (utime.ticks_diff(curr_time, start_time) > self.pattern[i]):
                if (i % 2) == 0: # if its an even index
                    t2ch1.pulse_width_percent(100) # turn led on
                else: #if odd index
                    t2ch1.pulse_width_percent(0) # turn led off
                i += 1 # add 1 to index

    def countdown(self, number = 3, interval = 1000):
        '''
        @brief counts down from a number

        Parameters
        ----------
        number : INTEGER, optional
            The number at which the countdown starts. The default is 3.
        interval : INTEGER, optional
            The interval at which the numbers print [ms]. The default is 1000.

        Returns
        -------
        None.

        '''
        count = utime.ticks_ms()
        x = number
        while x >= 0:
            curr = utime.ticks_ms()
            if (curr - count > interval):
                if x > 0:
                    print(x)
                elif x == 0:
                    print('Go!')
                count = utime.ticks_ms()
                x -= 1
        
    def toTimeSequence(self):
        '''
        @brief converts a string of morse code characters to a sequence of on and off times

        '''
        self.pattern = [0]
        for i in range(self.level):
            for x in self.morse_code[i]:
                if x == '-':
                    self.pattern.append(self.pattern[-1] + self.unit * 3)
                    self.pattern.append(self.pattern[-1] + self.unit)
                elif x == '.':
                    self.pattern.append(self.pattern[-1] + self.unit * 1)
                    self.pattern.append(self.pattern[-1] + self.unit)
                elif x == '|':
                    self.pattern[-1] += (self.unit * 2)
        self.pattern = self.pattern[0:(len(self.pattern)-1)]
        for i in range(self.level):
            self.round += self.word[i]
        
    def listen(self):
        '''
        @brief Takes user button input and stores it in a list of press and release times

        Returns
        -------
        None.

        '''
        user = []
        start_time = utime.ticks_ms()
        self.goVar = 0
        while len(user) <= len(self.pattern):
            curr_time = utime.ticks_ms()
            runtime = curr_time - start_time
            if self.goVar == 1:
                user.append(runtime)
                if (len(user)%2):
                    t2ch1.pulse_width_percent(100)
                else:
                    t2ch1.pulse_width_percent(0)
                for x in range(1, len(user)):
                    theor = self.pattern[x] - self.pattern[x-1]
                    exper = user[x] - user[x-1]
                    if abs(theor - exper) > self.tolerance:
                        self.state = self.S5_FAILURE
                        return
                    else:
                        pass
                self.goVar = 0
                if len(user) == len(self.pattern):
                    self.state = self.S6_ROUND_WIN
                    return
            
    
    def buttonPress(self, IRQ_src):
        '''
        @brief   Callback that sets flag goVar high
        @details This ExtInt callback is triggered by a button on the Nucleo and will
                 set a flag high, thus allowing control of multiple facets of the 
                 game.
        '''
        self.goVar = 1
            
#Pin that reads high when button is pressed and low when button is released
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

# Constructs an object of the taskBlinking class
game1 = simonSays(difficulty)


def celebrate(length = 8, period = 1):
    start = utime.ticks_ms()
    x = 0
    while x < length:
        curr = utime.ticks_ms()
        runtime = (curr - start)/1000
        if runtime >= period:
            start = utime.ticks_ms()
            x += 1
        t2ch1.pulse_width_percent(runtime*100*(1/period))
    t2ch1.pulse_width_percent(0)
        
        
#Interrupt handler for the callback triggered by the button press and release
buttonPress = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,
                         pull=pyb.Pin.PULL_NONE, callback=game1.buttonPress)


for N in range(10000000): # effectively while True
    game1.run() #Runs game